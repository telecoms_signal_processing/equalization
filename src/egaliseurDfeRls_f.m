function [e, y] = egaliseurDfeRls_f(trainingSymbols,...
                                  numTrainSymbols,...
                                  rxSig,...
                                  ReferenceTap,...
                                  sigConst,...
                                  nff,...
                                  nfb,... 
                                  lambda,...
                                  delta, ...
                                  weightUpdatePeriod, ...
                                  sps)
%- Description: 
% Inputs: 
%-         trainingSymbols : 1*N1 complex values, la s�quence d'entrainement
%-                   rxSig : 1*N complex values, le signal � �galiser
%-                sigConst : 1*C complex, la constellation utilis�e
%-                    initW: 1*L complex, les poids initiaux du filtre DFE
%-                  refTap : 1 uint, la prise de reference
%-                   lambda: 1 float ,le facteur d'oubli
%-                     nfb : 1 int, la taille du filtre de retour
%-                     nff : 1 int, la taille du filtre transverse
%-          numTrainSymbols: 1 int, nombre d'�chantillons de la seq d'entrainement
%-      weightUpdatePeriod : 1 uint, p�riode d'adaptation des poids
%-                    delta: 1 float, param�tre l'inverse de la matrice de corr�lation 
%-                 DumpFlag: 1 ?, param�tre le fait de logger par trame les
%                                 e/s d'un algo
% Outputs: 
%                        y: 1*N complex values, equalized signal, 
%                        e: 1*N complex values, error signal
%--------------------------------------------------------------------------

%- ToDo make it variable.
%refvec =[];
InputDelay = 0; 
ref = eps;
if nargin < 11
    sps = 1; %-InputSamplesPerSymbol
end 
adaptWeights = true;
trainingFlag = true;
    
totalNumTaps = nff + nfb;
u = zeros(totalNumTaps, 1); % TapDelayLine
w = zeros(totalNumTaps, 1);%w(ceil(length(w)/2)) = 1;

symbolCounter = 0;
adaptAfterTraining = true;
weightUpdateCounter = 0;
    
P = delta*eye(totalNumTaps);

outputLength = (length(rxSig) / sps);
y = zeros([outputLength 1], 'like', rxSig);
e = zeros([outputLength 1], 'like', rxSig);
lambdaInverse =(1/lambda);
    
    
activeDelay = ReferenceTap -1; %ceil((InputDelay+ReferenceTap)/sps)-1;
trainingDelayBufferLength = ceil((InputDelay+ReferenceTap)/sps)-1;%-1;
    
trainingDelayBuffer = struct('Length', ...
          trainingDelayBufferLength, ...
          'Buffer', ...
          zeros(trainingDelayBufferLength, 1, 'like', rxSig), ...
          'Pointer', 1);
for p = 1:outputLength 
    for idx=nff:-1:sps+1
        u(idx) = u(idx-sps);
    end
    for q=1:sps
        u(q,1) = rxSig((p-1)*sps+q,1);
    end
    if nfb > 0
        for idx=nff+nfb:-1:nff+2
            u(idx) = u(idx-1);
        end
        u(nff+1) = ref;
    end
    
    symbolCounter = symbolCounter + 1;
    weightUpdateCounter = weightUpdateCounter + 1;
    % Filter
    yTemp = w' * u;
  
  % Calculate error signal
  if symbolCounter <= numTrainSymbols
    if p <= length(trainingSymbols)
      dRef = trainingSymbols(p,1);
    else
      % If we ran out of training symbols, we need to push dummy
      % symbols until all the training is flushed out.
      dRef = trainingSymbols(1,1);
    end
    
    if trainingDelayBuffer.Length > 0
      ref = trainingDelayBuffer.Buffer(trainingDelayBuffer.Pointer);
      trainingDelayBuffer.Buffer(trainingDelayBuffer.Pointer) = dRef;
      trainingDelayBuffer.Pointer = trainingDelayBuffer.Pointer + 1;
      if trainingDelayBuffer.Pointer > trainingDelayBuffer.Length
        trainingDelayBuffer.Pointer = 1;
      end
    else
      ref = dRef;
    end
    
    if symbolCounter <= activeDelay
      % Use decisions as reference
      [~,idxa] = min(abs(sigConst - yTemp));
      ref = sigConst(idxa);
    end
  else
      % Decide on the received signal (Detector/Slicer)
      [~,idxa] = min(abs(sigConst - yTemp));
      ref = sigConst(idxa);
      if ~adaptAfterTraining
          adaptWeights = false;
      end
  end
  eTemp = ref - yTemp;
  
  if adaptWeights
      % Update tap weights
      if (trainingFlag == true) ...
          || (weightUpdateCounter == weightUpdatePeriod)
          weightUpdateCounter = 0;
          UP = u'*P;
          K = (P*u) / (lambda + UP*u);
          w = w + K*conj(eTemp);
          P = lambdaInverse * (P - K*UP);
      end
  end  
  % Assign output
  y(p,1) = yTemp;
  e(p,1) = eTemp;
end
