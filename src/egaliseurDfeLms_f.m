function [e, y] = egaliseurDfeLms_f(trainingSymbols,...
                                    numTrainSymbols,...
                                    rxSig,...
                                    ReferenceTap,...
                                    sigConst,...
                                    nff,...
                                    nfb,... 
                                    stepSize, ...
                                    weightUpdatePeriod)
%- Description : DFE equalizer implementation with LMS algorithm
%- Inputs: 
%
%-       trainingSymbols : 1*N1 complex values, la s�quence d'entrainement
%-                 rxSig : 1*N complex values,le signal � �galiser
%-              sigConst : 1*C complex values,la constellation utilis�e
%-                 initW : 1*L complex values, les poids initiaux du filtre DFE
%-                refTap : 1 uint,la prise de reference
%-                   nfb : 1 uint, la taille du filtre de retour
%-                   nff : 1 uint, la taille du filtre transverse
%-        numTrainSymbols: 1 uint, nombre d'�chantillons de la seq d'entrainement
%-               stepSize: 1 float, pas du lms
%-    weightUpdatePeriod : 1 uint, p�riode d'adaptation des poids 


% Outputs: 
%                    y  : 1*N complex values, output equalized signal, 
%                     e : 1*N complex values, error signal. 
%--------------------------------------------------------------------------
%- ToDo
%- add consistency checks  
% narginchk(3, 3);
    %- ToDo make it variable.
    InputDelay = 0; 
    ref = eps;
    sps = 1; %InputSamplesPerSymbol
    adaptWeights = true;
    trainingFlag = true; 
    totalNumTaps = nff + nfb;
    u = zeros(totalNumTaps, 1); % TapDelayLine
    w = zeros(totalNumTaps, 1);w(ceil(length(w)/2)) = 1;

    symbolCounter = 0;
    adaptAfterTraining = true;
    weightUpdateCounter = 0;

    outputLength = (length(rxSig) / sps);
    y = zeros([outputLength 1], 'like', rxSig);
    e = zeros([outputLength 1], 'like', rxSig);
    
    
    activeDelay = ReferenceTap -1; %ceil((InputDelay+ReferenceTap)/sps)-1;
    trainingDelayBufferLength = ceil((InputDelay+ReferenceTap)/sps)-1;%-1;
   trainingDelayBuffer = struct('Length', ...
          trainingDelayBufferLength, ...
          'Buffer', ...
          zeros(trainingDelayBufferLength, 1, 'like', rxSig), ...
          'Pointer', 1);
for p = 1:outputLength 
      % Update tap delay line

      for idx=nff:-1:sps+1
          u(idx) = u(idx-sps);
      end
      for q=1:sps
          u(q,1) = rxSig((p-1)*sps+q,1);
      end
      if nfb > 0
          for idx=nff+nfb:-1:nff+2
              u(idx) = u(idx-1);
          end
          u(nff+1) = ref;
      end
      
      symbolCounter = symbolCounter + 1;
      weightUpdateCounter = weightUpdateCounter + 1;
  
      % Filter
      yTemp = w' * u;
  
      % Calculate error signal
      if symbolCounter <= numTrainSymbols
          if p <= length(trainingSymbols)
              dRef = trainingSymbols(p,1);
          else
              % If we ran out of training symbols, we need to push dummy
              % symbols until all the training is flushed out.
              dRef = trainingSymbols(1,1);
          end
          
          if trainingDelayBuffer.Length > 0
              ref = trainingDelayBuffer.Buffer(trainingDelayBuffer.Pointer);
              trainingDelayBuffer.Buffer(trainingDelayBuffer.Pointer) = dRef;
              trainingDelayBuffer.Pointer = trainingDelayBuffer.Pointer + 1;
              if trainingDelayBuffer.Pointer > trainingDelayBuffer.Length
                  trainingDelayBuffer.Pointer = 1;
              end
          else
              ref = dRef;
          end
          if symbolCounter <= activeDelay
              % Use decisions as reference
              [~,idx] = min(abs(sigConst - yTemp));
              ref = sigConst(idx);
          end
      else
          % Decide on the received signal (Detector/Slicer)
          [~,idx] = min(abs(sigConst - yTemp));
          ref = sigConst(idx);
    
          if ~adaptAfterTraining
              adaptWeights = false;
          end
      end
      eTemp = ref - yTemp;
      if adaptWeights
          % Update tap weights
          if (trainingFlag == true) || (weightUpdateCounter == weightUpdatePeriod)
              w = w + stepSize*u*conj(eTemp);

          end
      end
      % Assign output
      y(p,1) = yTemp;
      e(p,1) = eTemp;
end
end
